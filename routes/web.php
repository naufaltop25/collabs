<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => ['auth']], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    
    Route::get('/user', 'UserController@index')->name('user');
    Route::get('/user/{id}', 'UserController@show')->name('user/show');
    Route::post('/user/getData', 'UserController@getData')->name('user/getData');
    Route::post('/user/store', 'UserController@store')->name('user/store');
    Route::post('/user/update', 'UserController@update')->name('user/update');
    Route::post('/user/password', 'UserController@changePassword')->name('user/password');
    Route::post('/user/delete/{id}', 'UserController@destroy')->name('user/delete');
    
    //Customer
    Route::get('/customer', 'CustomerController@index')->name('customer');
    Route::post('/customer/getData', 'CustomerController@getData')->name('customer/getData');
    Route::get('/customer/create', 'CustomerController@create')->name('customer/create');
    Route::post('/customer/store', 'CustomerController@store')->name('customer/store');
    Route::get('/customer/edit/{id}', 'CustomerController@edit')->name('customer/edit');
    Route::post('/customer/update', 'CustomerController@update')->name('customer/update');
    Route::post('/customer/delete/{id}', 'CustomerController@destroy')->name('customer/delete');
    
    //Product
    Route::get('/product', 'ProductController@index')->name('product');
    Route::get('/product/{id}', 'ProductController@show')->name('product/show');
    Route::post('/product/getData', 'ProductController@getData')->name('product/getData');
    Route::post('/product/store', 'ProductController@store')->name('product/store');
    Route::post('/product/update', 'ProductController@update')->name('product/update');
    Route::post('/product/password', 'ProductController@changePassword')->name('product/password');
    Route::post('/product/delete/{id}', 'ProductController@destroy')->name('product/delete');

    //Transaksi
    Route::get('/transaction', 'TransactionController@index')->name('transaction');
    Route::get('/transaction/show/{id}', 'TransactionController@show')->name('transaction/show');
    Route::post('/transaction/getData', 'TransactionController@getData')->name('transaction/getData');
    Route::get('/transaction/create', 'TransactionController@create')->name('transaction/create');
    Route::post('/transaction/store', 'TransactionController@store')->name('transaction/store');
    Route::get('/transaction/detail/{id}', 'TransactionController@detail')->name('transaction/detail');
    Route::get('/transaction/edit/{id}', 'TransactionController@edit')->name('transaction/edit');
    Route::post('/transaction/update', 'TransactionController@update')->name('transaction/update');
    Route::post('/transaction/delete/{id}', 'TransactionController@destroy')->name('transaction/delete');
    Route::post('/transaction/finish/{id}', 'TransactionController@finish')->name('transaction/finish');
});

