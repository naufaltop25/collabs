@extends('layout.admin')

@section('judul')
    Pelanggan
@endsection

@section('subjudul')
    Tambah Pelanggan
@endsection

@section('content')
    <div class="card col-md-12">
        <div class="card-body">
            <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">Tambah Pelanggan</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="{{ route('dashboard') }}">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('customer') }}">Daftar Pelanggan</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item active">Tambah Pelanggan
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('customer/store') }}" id="customerForm" name="customerForm" enctype="multipart/form-data">
                            @csrf
                                <div class="form-group row">
                                    <label for="name" class="col-md-2 form-control-label text-md-left">Nama<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input id="name" type="text" class="form-control" name="name" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="gender" class="col-md-2 form-control-label text-md-left">Jenis Kelamin<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <select id="gender" class="form-control gender" name="gender">
                                            <option></option>
                                            <option value="Pria">Pria</option>
                                            <option value="Wanita">Wanita</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-2 form-control-label text-md-left">Email<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input id="email" type="email" class="form-control" name="email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone_number" class="col-md-2 form-control-label text-md-left">Nomor HP<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input id="phone_number" type="text" class="form-control" name="phone_number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="address" class="col-md-2 form-control-label text-md-left">Alamat<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <textarea class="form-control" name="address" id="address" rows="5"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-md btn-primary with-loading" id="saveBtn" name="saveBtn">Simpan</button>
                            <a href="{{route('customer')}}" class="btn btn-md btn-default">Batal</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
$(function(){
    $("#gender").select2({
        theme: "default",
        placeholder: "Pilih Jenis Kelamin",
    });
    $('#saveBtn').click(function(e) {
      e.preventDefault();
      var isValid = $("#customerForm").valid();
      if(isValid){
        var url = "{{route('customer/store')}}";
        $('#saveBtn').text('Save...');
        $('#saveBtn').attr('disabled',true);
        var formData = new FormData($('#customerForm')[0]);
        $.ajax({
          url : url,
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {
            Swal.fire(
              (data.status) ? 'Berhasil' : 'Gagal',
              data.message,
              (data.status) ? 'success' : 'error'
            )
            $('#saveBtn').text('Save');
            $('#saveBtn').attr('disabled',false);
            (data.status)?window.location.href = "{{route('customer')}}":'';
          },
          error: function (data)
          {
            Swal.fire(
                'Ups, terjadi kesalahan',
                'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                'error'
            )
            $('#saveBtn').text('Save');
            $('#saveBtn').attr('disabled',false);
          }
        });
      }
    });
    $('#customerForm').validate({
      rules: {
        name: {
          required: true,
        },
        gender: {
          required: true,
        },
        email: {
          required: true,
          email: true
        },
        phone_number: {
          required: true,
        },
        address: {
          required: true,
        },
      },
      errorElement: 'em',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.validate').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
});
</script>
@endpush