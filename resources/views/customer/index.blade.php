@extends('layout.admin')

@section('judul')
    Pelanggan
@endsection

@section('subjudul')
    Daftar Pelanggan
@endsection

@section('content')
    <div class="card col-md-12">
        <div class="card-body">
            <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">Daftar Pelanggan</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="{{ route('dashboard') }}">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item active">
                                <a href="{{ route('customer') }}">Daftar Pelanggan</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('customer/create')}}" class="btn btn-primary btn-round"><i class="fa fa-plus"></i> Pelanggan</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="cutomersTable" class="display table table-striped table-hover" width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Homor HP</th>
                                            <th>Email</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Alamat</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
$(function(){
    let request = {
        start:0,
        length:10
    };
    var cutomersTable = $('#cutomersTable').DataTable({
        "language": {
          "info": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
          "lengthMenu":     "Menampilkan _MENU_ data",
          "search":         "Cari nama pelanggan:",
          "processing":     "Sedang mencari data...",
          "zeroRecords":    "Tidak ada data yang ditemukan",
          "paginate": {
              "next":       '<i class="fas fa-arrow-right"></i>',
              "previous":   '<i class="fas fa-arrow-left"></i>'
          }
        },
        "aaSorting": [],
        "ordering": false,
        "responsive": true,
        "serverSide": true,
        "lengthMenu": [[10, 25, 50, -1], [10 , 25 , 50 , "All"]],
          "ajax": {
              "url": "{{route('customer/getData')}}",
              "type": "POST",
              "headers":
              {
                  'X-CSRF-Token': $('input[name="_token"]').val()
              },
              "beforeSend": function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $('#secret').val());
              },
              "Content-Type": "application/json",
              "data": function(data) {
                  request.draw = data.draw;
                  request.start = data.start;
                  request.length = data.length;
                  request.searchkey = data.search.value || "";
                  return (request);
              },
          },
          "columns": [
              { "data": null,
                "width" : '5%',
                render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1;
                }  
              },
              {
                "data": "name",
                "width" : '15%',
                "defaultContent": "-"
              },
              {
                "data": "phone_number",
                "width" : '15%',
                "defaultContent": "-"
              },
              {
                "data": "email",
                "width" : '15%',
                "defaultContent": "-"
              },
              {
                "data": "gender",
                "width" : '10%',
                "defaultContent": "-"
              },
              {
                "data": "address",
                "width" : '25%',
                "defaultContent": "-"
              },
              {
                "data": "id",
                "width" : '15%',
                render: function(data, type, row) {
                  let btnEdit           = "";
                  let btnDelete         = "";
                  btnEdit += '<a href="/customer/edit/'+data+'" name="btnEdit" data-id="' + data + '" type="button" class="btn btn-warning btn-sm btnEdit" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>';
                    btnDelete = '<button name="btnDelete" data-id="' + data + '" type="button" class="btnDelete btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>';
                  return btnEdit+" "+btnDelete;
                },
              },
          ]
    });
    function reloadTable(){
        cutomersTable.ajax.reload(null,false); //reload datatable ajax 
    }
    $('#cutomersTable').on("click", ".btnDelete", function(){
        var id= $(this).attr('data-id');
        Swal.fire({
          title: 'Konfirmasi',
          text: "Anda akan menghapus pelanggan ini. Apa anda yakin akan melanjutkan ?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, saya yakin',
          cancelButtonText: 'Batal'
          }).then(function (result) {
          if (result.value) {
            var url = "{{route('customer/delete',['id'=>':id'])}}";
            url = url.replace(':id',id);
            $.ajax({
              headers:
              {
                'X-CSRF-Token': $('input[name="_token"]').val()
              },
              url: url,
              type: "POST",
              success: function (data) {
                Swal.fire(
                  (data.status) ? 'Berhasil' : 'Gagal',
                  data.message,
                  (data.status) ? 'success' : 'error'
                )
                reloadTable();
              },
              error: function(response) {
                Swal.fire(
                  'Ups, terjadi kesalahan',
                  'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                  'error'
                )
              }
            });
          }
        })
    });
});
</script>
@endpush