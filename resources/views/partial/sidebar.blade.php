<!-- Sidebar -->
<div class="sidebar sidebar-style-2">			
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="{{ auth()->user()->photo != '' ? asset('storage/images/user/' . auth()->user()->photo) : asset('admin/assets/img/profile.jpg') }}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            {{ Auth::user()->name }} 
                            <span class="user-level">{{ Auth::user()->email }} </span>
                        </span>
                    </a>
                </div>
            </div>
            <ul class="nav nav-primary">
                <li class="nav-item {{ (request()->is('/')) ? 'active' : '' }}">
                    <a href="{{ route('dashboard') }}">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
				</li>
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Menu</h4>
                </li>
                <li class="nav-item {{ (request()->is('user') || request()->is('user/*')) ? 'active' : '' }}">
                    <a href="{{ route('user') }}">
                        <i class="fas fa-user"></i>
                        <p>Daftar Pengguna</p>
                    </a>
                </li>
                <li class="nav-item {{ (request()->is('customer') || request()->is('customer/*')) ? 'active' : '' }}">
                    <a href="{{ route('customer') }}">
                        <i class="fas fa-users"></i>
                        <p>Daftar Pelanggan</p>
                    </a>
                </li>
                <li class="nav-item {{ (request()->is('product') || request()->is('product/*')) ? 'active' : '' }}">
                    <a href="{{ route('product') }}">
                        <i class="fas fa-box"></i>
                        <p>Daftar Produk</p>
                    </a>
                </li>
                <li class="nav-item {{ (request()->is('transaction') || request()->is('transaction/*')) ? 'active' : '' }}">
                    <a href="{{ route('transaction') }}">
                        <i class="fas fa-list"></i>
                        <p>Daftar Transaksi</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->