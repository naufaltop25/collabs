@extends('layouts.app')

@section('header')
    Login
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <img src="{{asset('admin/assets/img/bg-login.jpg')}}" alt="...">
                <div class="card-body p-5">
                    <h4>{{ __('Login') }}</h4>
        
                    <form action="{{ route('login') }}" method="post">
                        @csrf          
                        <div class="input-group mb-3 mt-4">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus aria-describedby="icon" placeholder="{{ __('Email Address') }}">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                            </div>
        
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
        
                        <div class="input-group mb-3">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" autocomplete="current-password" autofocus aria-describedby="icon" placeholder="{{ __('Password') }}">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="icon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                            </div>
        
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-check custom-control custom-checkbox mb-4">
                            <input class="form-check-input custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label custom-control-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
        
                        <button type="submit" class="btn btn-primary btn-block">
                            {{ __('Login') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
