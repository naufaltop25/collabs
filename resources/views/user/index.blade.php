@extends('layout.admin')

@section('judul')
    Pengguna
@endsection

@section('subjudul')
    Daftar Pengguna
@endsection

@section('content')
<div class="modal fade" id="userModal" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" id="userForm" name="userForm">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white" id="userModalLabel">Form Pengguna</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @csrf
        <input id="id" name="id" type="hidden" class="form-control">
        <div class="form-group">
          <label for="name" class="col-md-12 form-control-label text-md-left">Nama<span style="color:red;">*</span></label>
          <div class="col-md-12 validate">
              <input id="name" type="text" class="form-control" name="name">
          </div>
        </div>
        <div class="form-group">
          <label for="username" class="col-md-12 form-control-label text-md-left">Username<span style="color:red;">*</span></label>
          <div class="col-md-12 validate">
              <input id="username" type="text" class="form-control" name="username">
          </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-md-12 form-control-label text-md-left">Email<span style="color:red;">*</span></label>
            <div class="col-md-12 validate">
                <input id="email" type="email" class="form-control" name="email">
            </div>
        </div>
        <div id="showPassword" class="form-group">
            <label for="password" class="col-md-12 form-control-label text-md-left">Kata Sandi<span style="color:red;">*</span></label>
            <div class="col-md-12 validate">
                <input id="password" type="password" class="form-control" name="password"/>
            </div>
        </div>
        <div id="showConfirmPassword" class="form-group">
            <label for="password_confirm" class="col-md-12 form-control-label text-md-left">Konfirmasi Kata Sandi<span style="color:red;">*</span></label>
            <div class="col-md-12 validate">
                <input id="password_confirm" type="password" class="form-control" name="password_confirm"/>
            </div>
        </div>
        <div class="form-group">
            <label for="photo" class="col-md-12 form-control-label text-md-left">Upload Foto</label>
            <div class="col-md-12 validate">
                <input id="photo" type="file" class="form-control-file" name="photo">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm with-loading" id="createBtn" name="createBtn">Simpan</button>
        <button type="button" class="btn btn-default btn-sm btn-modal-dismiss" data-dismiss="modal">Tutup</button>
      </div>
       </form>
    </div>
  </div>
</div>
<div class="modal fade" id="changePasswordModal" role="dialog" aria-labelledby="changePasswordModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST"  action="{{ route('user/password') }}" id="changePasswordForm" name="changePasswordForm">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white" id="changePasswordModalLabel">Form Ganti Password</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @csrf
        <input id="user_id" type="hidden" class="form-control" name="id"/>
        <div class="form-group">
            <label for="newPassword" class="col-md-12 col-form-label text-md-left">Kata Sandi<span style="color:red;">*</span></label>
            <div class="col-md-12 validate">
                <input id="newPassword" type="password" class="form-control" name="password"/>
            </div>
        </div>
        <div class="form-group">
            <label for="newPasswordConfirm" class="col-md-12 col-form-label text-md-left">Konfirmasi Kata Sandi<span style="color:red;">*</span></label>
            <div class="col-md-12 validate">
                <input id="newPasswordConfirm" type="password" class="form-control" name="password_confirm"/>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm with-loading" id="passwordBtn" name="passwordBtn">Simpan</button>
        <button type="button" class="btn btn-default btn-sm btn-modal-dismiss" data-dismiss="modal">Tutup</button>
      </div>
       </form>
    </div>
  </div>
</div>
    <div class="card col-md-12">
        <div class="card-body">
            <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">Daftar Pengguna</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="{{ route('dashboard') }}">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item active">
                                <a href="/user">Daftar Pengguna</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" class="btn btn-primary btn-round" data-toggle="modal" data-target="#userModal" id="addNew" name="addNew"><i class="fa fa-plus"></i> Pengguna</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="usersTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
$(function(){
    let request = {
        start:0,
        length:10
    };
    var usersTable = $('#usersTable').DataTable({
        "language": {
          "info": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
          "lengthMenu":     "Menampilkan _MENU_ data",
          "search":         "Cari nama pengguna:",
          "processing":     "Sedang mencari data...",
          "zeroRecords":    "Tidak ada data yang ditemukan",
          "paginate": {
              "next":       '<i class="fas fa-arrow-right"></i>',
              "previous":   '<i class="fas fa-arrow-left"></i>'
          }
        },
        "aaSorting": [],
        "ordering": false,
        "responsive": true,
        "serverSide": true,
        "lengthMenu": [[10, 25, 50, -1], [10 , 25 , 50 , "All"]],
          "ajax": {
              "url": "{{route('user/getData')}}",
              "type": "POST",
              "headers":
              {
                  'X-CSRF-Token': $('input[name="_token"]').val()
              },
              "beforeSend": function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $('#secret').val());
              },
              "Content-Type": "application/json",
              "data": function(data) {
                  request.draw = data.draw;
                  request.start = data.start;
                  request.length = data.length;
                  request.searchkey = data.search.value || "";
                  return (request);
              },
          },
          "columns": [
              { "data": null,
                "width" : '5%',
                render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1;
                }  
              },
              {
                "data": "name",
                "width" : '25%',
                "defaultContent": "-"
              },
              {
                "data": "username",
                "width" : '20%',
                "defaultContent": "-"
              },
              {
                "data": "email",
                "width" : '20%',
                "defaultContent": "-"
              },
              {
                "data": "id",
                "width" : '18%',
                render: function(data, type, row) {
                  let btnEdit           = "";
                  let btnChangePassword = "";
                  let btnDelete         = "";
                    btnEdit = '<button name="btnEdit" data-id="' + data + '" type="button" class="btnEdit btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-user-edit"></i></button>';
                    btnChangePassword = '<button name="btnChangePassword" data-id="' + data + '" type="button" class="btnChangePassword btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Ganti Password"><i class="fa fa-user-lock"></i></button>';
                    btnDelete = '<button name="btnDelete" data-id="' + data + '" type="button" class="btnDelete btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>';
                  return btnEdit+" "+btnChangePassword+" "+btnDelete;
                },
              },
          ]
    });
    function reloadTable(){
        usersTable.ajax.reload(null,false); //reload datatable ajax 
    }
    $('#createBtn').click(function(e) {
        e.preventDefault();
        var isValid = $("#userForm").valid();
        if(isValid){
          $('#createBtn').text('Save...');
          $('#createBtn').attr('disabled',true);
          if(!isUpdate){
            var url = "{{route('user/store')}}";
          }else{
            var url = "{{route('user/update')}}";
          }
          var formData = new FormData($('#userForm')[0]);
          $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
              Swal.fire(
                    (data.status) ? 'Berhasil' : 'Gagal',
                    data.message,
                    (data.status) ? 'success' : 'error'
                )
              $('#createBtn').text('Save');
              $('#createBtn').attr('disabled',false);
              reloadTable();
              $('#userModal').modal('hide');
            },
            error: function (data)
            {
              Swal.fire(
                'Ups, terjadi kesalahan',
                'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                'error'
              )
              $('#createBtn').text('Save');
              $('#createBtn').attr('disabled',false);
            }
        });
        }
    });
    $('#passwordBtn').click(function(e) {
        e.preventDefault();
        var isValid = $("#changePasswordForm").valid();
        var formData = new FormData($('#changePasswordForm')[0]);
        if(isValid){
          $('#passwordBtn').text('Simpan...');
          $('#passwordBtn').attr('disabled',true);
          var url = "{{route('user/password')}}";
          $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            headers:
              {
              'X-CSRF-Token': $('input[name="_token"]').val()
              },
            success: function(data)
            {
              Swal.fire(
                (data.status) ? 'Berhasil' : 'Gagal',
                data.message,
                (data.status) ? 'success' : 'error'
              )
              $('#passwordBtn').text('Simpan');
              $('#passwordBtn').attr('disabled',false);
              reloadTable();
              $('#changePasswordModal').modal('hide');
            },
            error: function (data)
            {
              Swal.fire(
                'Ups, terjadi kesalahan',
                'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                'error'
              )
              $('#passwordBtn').text('Simpan');
              $('#passwordBtn').attr('disabled',false);
            }
          });
        }
    });
    $('#usersTable').on("click", ".btnEdit", function(){
        isUpdate = true;
        $('#userModal').modal('show');
        $('#showPassword').hide();
        $('#showConfirmPassword').hide();
        $("#password").rules('remove', 'required')
        $("#password_confirm").rules('remove', 'required')
        var id= $(this).attr('data-id');
        var url = "{{route('user/show',['id'=>':id'])}}";
        url = url.replace(':id', id);
        $.ajax({
          type: 'GET',
          url: url,
          success: function(response) {
              $('#id').val(response.data.id);
              $('#name').val(response.data.name);
              $('#username').val(response.data.username);
              $('#email').val(response.data.email);
            },
            error: function(){
              Swal.fire(
                'Ups, terjadi kesalahan',
                'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                'error'
              )
            },
        });
    });
    $('#usersTable').on("click", ".btnChangePassword", function(){
        $('#newPassword').val("");
        $('#newPasswordConfirm').val("");
        $('#changePasswordModal').modal('show');
        var id= $(this).attr('data-id');
        $('#user_id').val(id);
    });
    $('#usersTable').on("click", ".btnDelete", function(){
        var id= $(this).attr('data-id');
        Swal.fire({
          title: 'Konfirmasi',
          text: "Anda akan menghapus pengguna ini. Apa anda yakin akan melanjutkan ?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, saya yakin',
          cancelButtonText: 'Batal'
          }).then(function (result) {
          if (result.value) {
            var url = "{{route('user/delete',['id'=>':id'])}}";
            url = url.replace(':id',id);
            $.ajax({
              headers:
              {
                'X-CSRF-Token': $('input[name="_token"]').val()
              },
              url: url,
              type: "POST",
              success: function (data) {
                Swal.fire(
                  (data.status) ? 'Berhasil' : 'Gagal',
                  data.message,
                  (data.status) ? 'success' : 'error'
                )
                reloadTable();
              },
              error: function(response) {
                Swal.fire(
                  'Ups, terjadi kesalahan',
                  'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                  'error'
                )
              }
            });
          }
        })
    });
    $('#userForm').validate({
        rules: {
          username: {
            required: true,
          },
          name: {
            required: true,
          },
          email: {
            required: true,
            email: true,
          },
          password: {
            required: true,
            minlength: 8
          },
          password_confirm: {
            required: true,
            minlength: 8,
            equalTo: "#password"
          },
        },
        errorElement: 'em',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.validate').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
    });
    $('#changePasswordForm').validate({
        rules: {
          password: {
            required: true,
            minlength: 8
          },
          password_confirm: {
            required: true,
            minlength: 8,
            equalTo: "#newPassword"
          },
        },
        errorElement: 'em',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.validate').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
    });
    $('#addNew').on('click', function(){
        $('#name').val("");
        $('#username').val("");
        $('#email').val("");
        $('#showPassword').show();
        $("#password").rules("add", {
            required: true,
        });
        $('#password').val("");
        $('#password_confirm').val("");
        $('#showConfirmPassword').show();
        $("#password_confirm").rules("add", {
            required: true,
        });
        $('#photo').val("");
        isUpdate = false;
    });
});
</script>
@endpush