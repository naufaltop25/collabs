@extends('layout.admin')

@section('judul')
    Dashboard
@endsection

@section('subjudul')
    Selamat Datang Admin!
@endsection

@section('content')

    <div class="col-md-6">
        <div class="card full-height">
            <div class="card-body">
                <div class="card-title">Jumlah Pengguna</div>
                <div class="card-category">Informasi harian tentang jumlah pengguna dalam sistem</div>
                <div class="d-flex flex-wrap justify-content-between pb-2 pt-4">
                    <h1 style="font-size: 4rem">{{$jmlPengguna}}</h1>
                    <i class="fa fa-user" style="font-size: 5rem"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card full-height">
            <div class="card-body">
                <div class="card-title">Jumlah Produk</div>
                <div class="card-category">Informasi harian tentang jumlah produk dalam sistem</div>
                <div class="d-flex flex-wrap justify-content-between pb-2 pt-4">
                    <h1 style="font-size: 4rem">{{$jmlProduct}}</h1>
                    <i class="fa fa-box" style="font-size: 5rem"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Produk Unggulan</div>
            </div>
            <div class="card-body pb-2">
                @forelse ($product as $key => $item)    
                    <div class="d-flex">
                        <div class="avatar">
                            <img src="{{asset('admin/assets/img/bg-login.jpg')}}" alt="..." class="avatar-img rounded-circle">
                        </div>
                        <div class="flex-1 pt-1 ml-3">
                            <h6 class="fw-bold mb-1">{{$item->name}}</h6>
                            <small class="text-muted">{{Str::limit($item->note, 50)}}</small>
                        </div>
                        {{-- <div class="d-flex ml-auto align-items-center mb-4">
                            <h3 class="text-info fw-bold">$17</h3>
                        </div> --}}
                    </div>
                    <div class="separator-dashed"></div>
                @empty
                    <h1>Data tidak ada</h1>
                @endforelse
                <a href="{{ route('product') }}" style="text-decoration: none;">Lihat Detail</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <div class="card-title fw-mediumbold">Daftar Pelanggan</div>
            </div>
            <div class="card-body pb-2">
                @forelse ($customer as $key => $item)
                    <div class="d-flex">
                        <div class="avatar">
                            <img src="{{asset('admin/assets/img/guest.png')}}" alt="img" class="avatar-img rounded-circle">
                        </div>
                        <div class="flex-1 pt-1 ml-3">
                            <h6 class="fw-bold mb-1">{{$item->name}}</h6>
                            <small class="text-muted">{{$item->email}}</small>
                        </div>
                    </div>
                    <div class="separator-dashed"></div>
                @empty
                    <h1>Data tidak ada</h1>
                @endforelse
                <a href="{{ route('customer') }}" style="text-decoration: none;">Lihat Detail</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-primary">
            <div class="card-header">
                <div class="card-title">Jumlah Transaksi</div>
                <div class="card-category">Informasi harian tentang jumlah Transaksi dalam sistem</div>
            </div>
            <div class="card-body pb-0">
                <div class="mb-4 mt-2">
                    <h1>{{App\Http\Controllers\TransactionController::rupiah($jmlTransaction)}}</h1>
                </div>
            </div>
        </div>

    
@endsection
