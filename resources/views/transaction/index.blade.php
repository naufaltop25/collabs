@extends('layout.admin')

@section('judul')
    Transaksi
@endsection

@section('subjudul')
    Daftar Transaksi
@endsection

@section('content')
    <div class="card col-md-12">
        <div class="card-body">
            <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">Daftar Transaksi</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="{{ route('dashboard') }}">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item active">
                                <a href="{{ route('transaction') }}">Daftar Transaksi</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('transaction/create')}}" class="btn btn-primary btn-round"><i class="fa fa-plus"></i> Transaksi</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="transactionsTable" class="display table table-striped table-hover" width="100%">
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th>No Transaksi</th>
                                            <th>Pelanggan</th>
                                            <th>Produk</th>
                                            <th>Tanggal</th>
                                            <th>Jumlah</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                        <th colspan="5">Total Jumlah: {{App\Http\Controllers\TransactionController::rupiah($count)}}</th>
                                      </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
$(function(){
    let request = {
        start:0,
        length:10
    };
    var transactionsTable = $('#transactionsTable').DataTable({
        "language": {
          "info": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
          "lengthMenu":     "Menampilkan _MENU_ data",
          "search":         "Cari nomor transaksi:",
          "processing":     "Sedang mencari data...",
          "zeroRecords":    "Tidak ada data yang ditemukan",
          "paginate": {
              "next":       '<i class="fas fa-arrow-right"></i>',
              "previous":   '<i class="fas fa-arrow-left"></i>'
          }
        },
        "aaSorting": [],
        "ordering": false,
        "responsive": true,
        "serverSide": true,
        "lengthMenu": [[10, 25, 50, -1], [10 , 25 , 50 , "All"]],
          "ajax": {
              "url": "{{route('transaction/getData')}}",
              "type": "POST",
              "headers":
              {
                  'X-CSRF-Token': $('input[name="_token"]').val()
              },
              "beforeSend": function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $('#secret').val());
              },
              "Content-Type": "application/json",
              "data": function(data) {
                  request.draw = data.draw;
                  request.start = data.start;
                  request.length = data.length;
                  request.searchkey = data.search.value || "";
                  return (request);
              },
          },
          "columns": [
              {
                "data": "transaction_number",
                "width" : '15%',
                "defaultContent": "-",
                render: function (data, type, row) {
                    return "<div class='text-wrap'>" + data + "</div>";
                },
              },
              {
                "data": "customer.name",
                "width" : '15%',
                "defaultContent": "-",
                render: function (data, type, row) {
                    return "<div class='text-wrap'>" + data + "</div>";
                },
              },
              {
                "data": "product.name",
                "width" : '15%',
                "defaultContent": "-",
                render: function (data, type, row) {
                    return "<div class='text-wrap'>" + data + "</div>";
                },
              },
              {
                "data": "date",
                "width" : '10%',
                "defaultContent": "-",
                render: function (data, type, row) {
                    return "<div class='text-wrap'>" + moment(data).locale('id').format("DD MMMM YYYY") + "</div>";
                },
              },
              {
                "data": "amount",
                "width" : '10%',
                "defaultContent": "-",
                render: function (data, type, row) {
                    return "<div class='text-wrap'>Rp. " + currency(data) + ",00</div>";
                },
              },
              {
                "data": "status",
                "width" : '10%',
                "defaultContent": "-",
                render: function (data, type, row) {
                    let status = (data)?'Selesai':'Belum Selesai'
                    return "<div class='text-wrap'>" + status + "</div>";
                },
              },
              {
                "data": "id",
                "width" : '15%',
                render: function(data, type, row) {
                  let btnFinish   = "";
                  let btnDetail   = "";
                  let btnEdit   = "";
                  let btnDelete = "";
                  if (!row.status) {
                    btnFinish = '<button name="btnFinish" data-id="' + data + '" type="button" class="btnFinish btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Selesai"><i class="fas fa-check-double"></i></button>';
                    
                    btnEdit += '<a href="/transaction/edit/'+data+'" name="btnEdit" data-id="' + data + '" type="button" class="btn btn-warning btn-sm btnEdit" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>';
                    btnDelete = '<button name="btnDelete" data-id="' + data + '" type="button" class="btnDelete btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>';
                    }
                    btnDetail += '<a href="/transaction/detail/'+data+'" name="btnDetail" data-id="' + data + '" type="button" class="btn btn-primary btn-sm btnDetail" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-eye"></i></a>';
                  return btnFinish+" "+btnDetail+" "+btnEdit+" "+btnDelete;
                },
              },
          ]
    });
    function reloadTable(){
        transactionsTable.ajax.reload(null,false); //reload datatable ajax 
    }
    $('#transactionsTable').on("click", ".btnDelete", function(){
        var id= $(this).attr('data-id');
        Swal.fire({
          title: 'Konfirmasi',
          text: "Anda akan menghapus transaksi ini. Apa anda yakin akan melanjutkan ?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, saya yakin',
          cancelButtonText: 'Batal'
          }).then(function (result) {
          if (result.value) {
            var url = "{{route('transaction/delete',['id'=>':id'])}}";
            url = url.replace(':id',id);
            $.ajax({
              headers:
              {
                'X-CSRF-Token': $('input[name="_token"]').val()
              },
              url: url,
              type: "POST",
              success: function (data) {
                Swal.fire(
                  (data.status) ? 'Berhasil' : 'Gagal',
                  data.message,
                  (data.status) ? 'success' : 'error'
                )
                reloadTable();
              },
              error: function(response) {
                Swal.fire(
                  'Ups, terjadi kesalahan',
                  'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                  'error'
                )
              }
            });
          }
        })
    });
    $('#transactionsTable').on("click", ".btnFinish", function(){
      var id= $(this).attr('data-id');
      Swal.fire({
        title: 'Konfirmasi',
        text: "Anda akan menyelesaikan transaksi ini. Apa anda yakin akan melanjutkan ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "Yes, I'm sure",
        cancelButtonText: 'No'
        }).then(function (result) {
        if (result.value) {
          var url = "{{route('transaction/finish',['id'=>':id'])}}";
          url = url.replace(':id',id);
          $.ajax({
            headers:
            {
              'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: url,
            type: "POST",
            success: function (data) {
              Swal.fire(
                (data.status) ? 'Success' : 'Error',
                data.message,
                (data.status) ? 'success' : 'error'
              )
              reloadTable();
            },
            error: function(response) {
              Swal.fire(
                'Ups, terjadi kesalahan',
                'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                'error'
              )
            }
          });
        }
      })
    });
    function currency(num){
      var str = num.toString().split('.');
      if (str[0].length >= 3) {
          str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1.');
      }
      if (str[1] && str[1].length >= 3) {
          str[1] = str[1].replace(/(\d{3})/g, '$1 ');
      }
      return str.join('.');
    }
});
</script>
@endpush