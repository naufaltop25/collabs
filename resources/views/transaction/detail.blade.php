@extends('layout.admin')

@section('judul')
    Transaksi
@endsection

@section('subjudul')
    Detail Transaksi
@endsection

@section('content')
    <div class="card col-md-12">
        <div class="card-body">
            <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">Detail Transaksi</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="{{ route('dashboard') }}">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('transaction') }}">Daftar Transaksi</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item active">Detail Transaksi
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('transaction/store') }}" id="transactionForm" name="transactionForm" enctype="multipart/form-data">
                            @csrf
                                <input id="id" type="hidden" value="{{$id}}" class="form-control" name="id">
                                <div class="form-group row">
                                    <label for="date" class="col-md-2 form-control-label text-md-left">Tanggal<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input readonly id="date" type="text" class="form-control date" name="date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="customer_id" class="col-md-2 form-control-label text-md-left">Pelanggan<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <select readonly id="customer_id" class="form-control customer_id" name="customer_id">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="product_id" class="col-md-2 form-control-label text-md-left">Produk<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <select readonly id="product_id" class="form-control product_id" name="product_id">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="price" class="col-md-2 form-control-label text-md-left">Harga<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input readonly id="price" type="text" class="form-control" name="price" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="weight" class="col-md-2 form-control-label text-md-left">Berat<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input readonly id="weight" min="1" type="number" class="form-control" name="weight" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="amount" class="col-md-2 form-control-label text-md-left">Jumlah<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input readonly readonly id="amount" type="text" class="form-control" name="amount" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="note" class="col-md-2 form-control-label text-md-left">Catatan</label>
                                    <div class="col-md-4 validate">
                                        <textarea readonly class="form-control" name="note" id="note" rows="5"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('transaction')}}" class="btn btn-md btn-default">Batal</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
$(function(){
    var id = $('#id').val();
    show(id);
    function currency(num){
      var str = num.toString().split('.');
      if (str[0].length >= 3) {
          str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1.');
      }
      if (str[1] && str[1].length >= 3) {
          str[1] = str[1].replace(/(\d{3})/g, '$1 ');
      }
      return str.join('.');
    }
    function show(id) {
      var url = "{{route('transaction/show',['id'=>':id'])}}";
      url = url.replace(':id', id);
      $.ajax({
        type: 'GET',
        url: url,
        success: function(response) {
          $('#date').val(response.data.date);
          var customer_id = (response.data.customer_id)?new Option(response.data.customer.name, response.data.customer.id, true, true):null;
          $('#customer_id').append(customer_id).trigger('change')
          var product_id = (response.data.product_id)?new Option(response.data.product.name, response.data.product.id, true, true):null;
          $('#product_id').append(product_id).trigger('change')
          $('#price').val(currency(response.data.product.price));
          $('#weight').val(response.data.weight);
          $('#amount').val(currency(response.data.amount));
          $('#note').val(response.data.note);
        },
        error: function(){
          Swal.fire(
            'Error',
            'A system error has occurred. please try again later.',
            'error'
          )
        },
      });
    }
});
</script>
@endpush