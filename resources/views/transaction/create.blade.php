@extends('layout.admin')

@section('judul')
    Transaksi
@endsection

@section('subjudul')
    Tambah Transaksi
@endsection

@section('content')
    <div class="card col-md-12">
        <div class="card-body">
            <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">Tambah Transaksi</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="{{ route('dashboard') }}">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('transaction') }}">Daftar Transaksi</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item active">Tambah Transaksi
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('transaction/store') }}" id="transactionForm" name="transactionForm" enctype="multipart/form-data">
                            @csrf
                                <div class="form-group row">
                                    <label for="date" class="col-md-2 form-control-label text-md-left">Tanggal<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input id="date" type="text" class="form-control date" name="date" style="background-color: #fff">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="customer_id" class="col-md-2 form-control-label text-md-left">Pelanggan<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <select id="customer_id" class="form-control customer_id" name="customer_id">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="product_id" class="col-md-2 form-control-label text-md-left">Produk<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <select id="product_id" class="form-control product_id" name="product_id">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="price" class="col-md-2 form-control-label text-md-left">Harga<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input readonly id="price" type="text" class="form-control" name="price" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="weight" class="col-md-2 form-control-label text-md-left">Berat<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input id="weight" min="1" type="number" class="form-control" name="weight" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="amount" class="col-md-2 form-control-label text-md-left">Jumlah<span style="color:red;">*</span></label>
                                    <div class="col-md-4 validate">
                                        <input readonly id="amount" type="text" class="form-control" name="amount" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="note" class="col-md-2 form-control-label text-md-left">Catatan</label>
                                    <div class="col-md-4 validate">
                                        <textarea class="form-control" name="note" id="note" rows="5"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-md btn-primary with-loading" id="saveBtn" name="saveBtn">Simpan</button>
                            <a href="{{route('transaction')}}" class="btn btn-md btn-default">Batal</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
$(function(){
    $('.date').flatpickr({
      dateFormat: "Y-m-d"
    });
    $("#date").prop('readonly', false);
    $("#customer_id").select2({
      placeholder: "Pilih Pelanggan",
      ajax: {
          url: "{{route('customer/getData')}}",
          dataType: 'json',
          headers: {
              'X-CSRF-Token': $('input[name="_token"]').val(),
              'Content-Type': 'application/json',
              'Accept': 'application/json',
          },
          method: 'POST',
          delay: 250,
          destroy: true,
          data: function(params) {
              var query = {
                  searchkey: params.term || '',
                  start: 0,
                  length: -1,
              }
              return JSON.stringify(query);
          },
          processResults: function(data) {
              var result = {
                  results: [],
                  more: false
              };
              if (data && data.data) {
                  $.each(data.data, function() {
                      result.results.push({
                          id: this.id,
                          text: this.name
                      });
                  })
              }
              return result;
          },
          cache: false
      },
    });
    $("#product_id").select2({
      placeholder: "Pilih Product",
      ajax: {
          url: "{{route('product/getData')}}",
          dataType: 'json',
          headers: {
              'X-CSRF-Token': $('input[name="_token"]').val(),
              'Content-Type': 'application/json',
              'Accept': 'application/json',
          },
          method: 'POST',
          delay: 250,
          destroy: true,
          data: function(params) {
              var query = {
                  searchkey: params.term || '',
                  start: 0,
                  length: -1,
              }
              return JSON.stringify(query);
          },
          processResults: function(data) {
              var result = {
                  results: [],
                  more: false
              };
              if (data && data.data) {
                  $.each(data.data, function() {
                      result.results.push({
                          id: this.id,
                          text: this.name
                      });
                  })
              }
              return result;
          },
          cache: false
      },
    }).on("change", function(e) {
      let id = $(this).val();
      let url = "{{route('product/show',['id'=>':id'])}}";
      url = url.replace(':id', id);
       $.ajax({
          type: 'GET',
          url: url,
          success: function(response) {
            $('#price').val(currency(response.data.price))
          },
          error: function(){
            Swal.fire(
                'Error',
                'A system error has occurred. please try again later.',
                'error'
              )
          },
        });
    });
    $("#weight").on("keyup", function(event) {
      let price = $("#price").val();
      price = price.replace(/\./g, '');
      price = parseInt(price) || 0;

      let weight = $(this).val();
      weight = parseFloat(weight) || 0;

      let amount = price * weight;
      $('#amount').val(currency(amount));
    });
    $('#saveBtn').click(function(e) {
      e.preventDefault();
      var isValid = $("#transactionForm").valid();
      if(isValid){
        var url = "{{route('transaction/store')}}";
        $('#saveBtn').text('Save...');
        $('#saveBtn').attr('disabled',true);
        var formData = new FormData($('#transactionForm')[0]);
        $.ajax({
          url : url,
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data)
          {
            Swal.fire(
              (data.status) ? 'Berhasil' : 'Gagal',
              data.message,
              (data.status) ? 'success' : 'error'
            )
            $('#saveBtn').text('Save');
            $('#saveBtn').attr('disabled',false);
            (data.status)?window.location.href = "{{route('transaction')}}":'';
          },
          error: function (data)
          {
            Swal.fire(
                'Ups, terjadi kesalahan',
                'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                'error'
            )
            $('#saveBtn').text('Save');
            $('#saveBtn').attr('disabled',false);
          }
        });
      }
    });
    $('#transactionForm').validate({
      rules: {
        date: {
          required: true,
        },
        customer_id: {
          required: true,
        },
        product_id: {
          required: true,
        },
        weight: {
          required: true,
        },
        amount: {
          required: true,
        },
      },
      errorElement: 'em',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.validate').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    function currency(num){
      var str = num.toString().split('.');
      if (str[0].length >= 3) {
          str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1.');
      }
      if (str[1] && str[1].length >= 3) {
          str[1] = str[1].replace(/(\d{3})/g, '$1 ');
      }
      return str.join('.');
    }
});
</script>
@endpush