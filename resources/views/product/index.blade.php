@extends('layout.admin')

@section('judul')
    Produk
@endsection

@section('subjudul')
    Daftar Produk
@endsection

@section('content')
<div class="modal fade" id="productModal" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" id="productForm" name="productForm">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white" id="productModalLabel">Form Produk</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @csrf
        <input id="id" name="id" type="hidden" class="form-control">
        <div class="form-group">
          <label for="name" class="col-md-12 form-control-label text-md-left">Nama<span style="color:red;">*</span></label>
          <div class="col-md-12 validate">
              <input id="name" type="text" class="form-control" name="name" autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <label for="price" class="col-md-12 form-control-label text-md-left">Harga<span style="color:red;">*</span></label>
          <div class="col-md-12 validate">
              <input id="price" type="number" min="0" class="form-control" name="price" autocomplete="off">
          </div>
        </div>
        <div class="form-group">
            <label for="note" class="col-md-12 form-control-label text-md-left">Deskripsi</label>
            <div class="col-md-12">
                {{-- <input id="note" type="note" class="form-control" name="note"> --}}
                <textarea name="note" id="note" cols="30" rows="10" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group">
          <label for="photo" class="col-md-12 form-control-label text-md-left">Upload Foto</label>
          <div class="col-md-12 validate">
              <input id="photo" type="file" class="form-control-file" name="photo">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm with-loading" id="createBtn" name="createBtn">Simpan</button>
        <button type="button" class="btn btn-default btn-sm btn-modal-dismiss" data-dismiss="modal">Tutup</button>
      </div>
       </form>
    </div>
  </div>
</div>
    <div class="card col-md-12">
        <div class="card-body">
            <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">Daftar Produk</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="{{ route('dashboard') }}">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item active">
                                <a href="/user">Daftar Produk</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" class="btn btn-primary btn-round" data-toggle="modal" data-target="#productModal" id="addNew" name="addNew"><i class="fa fa-plus"></i> Produk</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="productsTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr class="bg-primary text-white">
                                            <th>No</th>
                                            <th>Gambar</th>
                                            <th>Nama</th>
                                            <th>Harga</th>
                                            <th>Deskripsi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@push('js')
<script type="text/javascript">
$(function(){
    let request = {
        start:0,
        length:10
    };
    var productsTable = $('#productsTable').DataTable({
        "language": {
          "info": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
          "lengthMenu":     "Menampilkan _MENU_ data",
          "search":         "Cari nama Produk:",
          "processing":     "Sedang mencari data...",
          "zeroRecords":    "Tidak ada data yang ditemukan",
          "paginate": {
              "next":       '<i class="fas fa-arrow-right"></i>',
              "previous":   '<i class="fas fa-arrow-left"></i>'
          }
        },
        "aaSorting": [],
        "ordering": false,
        "responsive": true,
        "serverSide": true,
        "lengthMenu": [[10, 25, 50, -1], [10 , 25 , 50 , "All"]],
          "ajax": {
              "url": "{{route('product/getData')}}",
              "type": "POST",
              "headers":
              {
                  'X-CSRF-Token': $('input[name="_token"]').val()
              },
              "beforeSend": function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + $('#secret').val());
              },
              "Content-Type": "application/json",
              "data": function(data) {
                  request.draw = data.draw;
                  request.start = data.start;
                  request.length = data.length;
                  request.searchkey = data.search.value || "";
                  return (request);
              },
          },
          "columns": [
              { "data": null,
                "width" : '5%',
                render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1;
                }  
              },
              {   
                "data":"photo",
                "width" : '10%',
                render: function(data, type, row) {
                    // var img = '<img src="assets/images/logo.png" width="100%" alt="...">';
                    if (row.photo) {
                        var img = '<img src="storage/images/product/'+row.photo+'" width="100%" alt="...">';
                    }
                    return img;
                },
              },
              {
                "data": "name",
                "width" : '25%',
                "defaultContent": "-"
              },
              {
                "data": "price",
                "width" : '20%',
                "defaultContent": "-"
              },
              {
                "data": "note",
                "width" : '20%',
                "defaultContent": "-"
              },
              {
                "data": "id",
                "width" : '18%',
                render: function(data, type, row) {
                  let btnEdit           = "";
                  let btnDelete         = "";
                    btnEdit = '<button name="btnEdit" data-id="' + data + '" type="button" class="btnEdit btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-user-edit"></i></button>';
                    btnDelete = '<button name="btnDelete" data-id="' + data + '" type="button" class="btnDelete btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>';
                  return btnEdit+" "+btnDelete;
                },
              },
          ]
    });
    function reloadTable(){
        productsTable.ajax.reload(null,false); //reload datatable ajax 
    }
    $('#createBtn').click(function(e) {
        e.preventDefault();
        var isValid = $("#productForm").valid();
        if(isValid){
          $('#createBtn').text('Save...');
          $('#createBtn').attr('disabled',true);
          if(!isUpdate){
            var url = "{{route('product/store')}}";
          }else{
            var url = "{{route('product/update')}}";
          }
          var formData = new FormData($('#productForm')[0]);
          $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data)
            {
              Swal.fire(
                    (data.status) ? 'Berhasil' : 'Gagal',
                    data.message,
                    (data.status) ? 'success' : 'error'
                )
              $('#createBtn').text('Save');
              $('#createBtn').attr('disabled',false);
              reloadTable();
              $('#productModal').modal('hide');
            },
            error: function (data)
            {
              Swal.fire(
                'Ups, terjadi kesalahan',
                'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                'error'
              )
              $('#createBtn').text('Save');
              $('#createBtn').attr('disabled',false);
            }
        });
        }
    });
    $('#productsTable').on("click", ".btnEdit", function(){
        isUpdate = true;
        $('#productModal').modal('show');
        var id= $(this).attr('data-id');
        var url = "{{route('product/show',['id'=>':id'])}}";
        url = url.replace(':id', id);
        $.ajax({
          type: 'GET',
          url: url,
          success: function(response) {
              $('#id').val(response.data.id);
              $('#name').val(response.data.name);
              $('#price').val(response.data.price);
              $('#note').val(response.data.note);
              $('#photo').val(response.data.photo);
            },
            error: function(){
              Swal.fire(
                'Ups, terjadi kesalahan',
                'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                'error'
              )
            },
        });
    });
    $('#productsTable').on("click", ".btnDelete", function(){
        var id= $(this).attr('data-id');
        Swal.fire({
          title: 'Konfirmasi',
          text: "Anda akan menghapus Produk ini. Apa anda yakin akan melanjutkan ?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, saya yakin',
          cancelButtonText: 'Batal'
          }).then(function (result) {
          if (result.value) {
            var url = "{{route('product/delete',['id'=>':id'])}}";
            url = url.replace(':id',id);
            $.ajax({
              headers:
              {
                'X-CSRF-Token': $('input[name="_token"]').val()
              },
              url: url,
              type: "POST",
              success: function (data) {
                Swal.fire(
                  (data.status) ? 'Berhasil' : 'Gagal',
                  data.message,
                  (data.status) ? 'success' : 'error'
                )
                reloadTable();
              },
              error: function(response) {
                Swal.fire(
                  'Ups, terjadi kesalahan',
                  'Tidak dapat menghubungi server, mohon coba beberapa saat lagi',
                  'error'
                )
              }
            });
          }
        })
    });
    $('#productForm').validate({
        rules: {
          name: {
            required: true,
          },
          price: {
            required: true,
          },
        },
        errorElement: 'em',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.validate').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
    });
    $('#addNew').on('click', function(){
        $('#name').val("");
        $('#price').val("");
        $('#note').val("");
        $('#photo').val("");
        isUpdate = false;
    });
});
</script>
@endpush