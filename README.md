## Final Project

## Group 2 - Kelompok 1

## Anggota Kelompok

- Naufal Mumtaz Adzaki
- Ari Dwiputra
- Syaroni

## Tema Project

Website Laundry

- Email Adress = admin@gmail.com
- Password = admin

# ERD
![Screenshot_from_2022-03-13_12-36-38](/uploads/e569be76090d9e94857ab514c4b18372/Screenshot_from_2022-03-13_12-36-38.png)

## Link Video

- Link Demo Aplikasi : [https://youtu.be/UzCsDjVWGcw](https://youtu.be/UzCsDjVWGcw)
- Link Deploy : [https://tkangbersih.pinjamanberkah.com/](https://tkangbersih.pinjamanberkah.com/)
