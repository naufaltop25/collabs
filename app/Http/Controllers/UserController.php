<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index() {
        return view('user.index');
    }
    public function getData(Request $request){
        $keyword = $request['searchkey'];

        $users = User::select()
            ->offset($request['start'])
            ->limit(($request['length'] == -1) ? User::count() : $request['length'])
            ->when($keyword, function ($query, $keyword) {
                return $query->where('name', 'like', '%'. $keyword . '%');
            })
            ->oldest('name')
            ->get();

        $usersCounter = User::select()
        ->when($keyword, function ($query, $keyword) {
            return $query->where('name', 'like', '%'. $keyword . '%');
        })
        ->count();
        $response = [
            'status'          => true,
            'code'            => '',
            'message'         => '',
            'draw'            => $request['draw'],
            'recordsTotal'    => User::count(),
            'recordsFiltered' => $usersCounter,
            'data'            => $users,
        ];
        return $response;
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data     = ['status' => false, 'code' => 'EC001', 'message' => 'Pengguna gagal ditambah'];
            $fileName = Str::random(20);
            $path     = 'images/user/';
            if(User::where('email', $request['email'])->exists()){
                return $data = ['status' => false, 'code' => 'EC002', 'message' => 'Email sudah digunakan'];
            }
            if(User::where('username', $request['username'])->exists()){
                return $data = ['status' => false, 'code' => 'EC002', 'message' => 'Username sudah digunakan'];
            }
            $validator = Validator::make($request->all(), [
                'photo' => 'image|mimes:jpg,jpeg,png|max:2048',
            ], [
                'photo' => 'File tidak boleh lebih dari 2MB, dengan format jpg, jpeg, png',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'code' => 'EC001', 'message' => 'File tidak boleh lebih dari 2MB, dengan format jpg, jpeg, png']);
            }
            if($request->file('photo') != null){
                $extension = $request->file('photo')->extension();
                $photoName = $fileName.'.'.$extension;
                Storage::disk('public')->putFileAs($path,$request->file('photo'), $fileName.".".$extension);
            }else{
                $photoName = null;
            }
            $user              = new User;
            $user->name        = $request['name'];
            $user->username    = $request['username'];
            $user->email       = $request['email'];
            $user->photo       = $photoName;
            $user->password    = Hash::make($request['password']);
            $user->save();

            if($user){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Pengguna berhasil ditambah'];
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem '.$ex];
        }
        return $data;
    }
    public function show($id)
    {
        try {
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Pengguna gagal didapatkan'];
            $user = User::where('id', $id)->first();
            if ($user) {
                $data = ['status'=> true, 'code'=> 'EEC001', 'message'=> 'Pengguna berhasil didapatkan','data'=> $user];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $data     = ['status' => false, 'code' => 'EC001', 'message' => 'Pengguna gagal diperbaharui'];
            $fileName = Str::random(20);
            $path     = 'images/user/';
            $user     = User::find($request->id);
            if ($user->email != $request['email']) {
                if(User::where('email', $request['email'])->exists()){
                    return $data = ['status' => false, 'code' => 'EC002', 'message' => 'Email sudah digunakan'];
                }
            }
            if ( $user->username != $request['username']) {
                if(User::where('username', $request['username'])->exists()){
                    return $data = ['status' => false, 'code' => 'EC002', 'message' => 'Username sudah digunakan'];
                }
            }
            $validator = Validator::make($request->all(), [
                'photo' => 'image|mimes:jpg,jpeg,png|max:2048',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => 'File tidak boleh lebih dari 2MB, dengan format jpg, jpeg, png']);
            }
            if($request->file('photo') != null){
                $extension = $request->file('photo')->extension();
                $photoName = $fileName.'.'.$extension;
                Storage::disk('public')->putFileAs($path,$request->file('photo'), $fileName.".".$extension);
            }else{
                $photoName = $user->photo;
            }
            
            $user           = User::find($request->id);
            $user->username = $request['username'];
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->photo    = $photoName;
            $user->save();

            if($user){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Pengguna berhasil diperbaharui'];
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
    public function changePassword(Request $request)
    {
       try {
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Password gagal diperbaharui'];
            
            $update = User::where('id', $request['id'])->update([
                'password' => Hash::make($request['password']),
            ]);
            if($update){
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Password berhasil diperbaharui'];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data; 
    }
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Pengguna gagal dihapus'];
            
            $user         = User::find($id);
            $user->delete();
            if($user){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Pengguna berhasil dihapus'];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
}
