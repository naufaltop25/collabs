<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Transaction;

class TransactionController extends Controller
{
    public function index() 
    {
        $count = Transaction::where('status', true)->sum('amount');
        return view('transaction.index', compact('count'));
    }
    public static function rupiah($angka){
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
    }
    public function getData(Request $request){
        $keyword = $request['searchkey'];

        $transactions = Transaction::select()
            ->offset($request['start'])
            ->limit(($request['length'] == -1) ? Transaction::count() : $request['length'])
            ->with('customer', 'product')
            ->when($keyword, function ($query, $keyword) {
                return $query->where('transaction_number', 'like', '%'. $keyword . '%');
            })
            ->latest()
            ->get();

        $transactionsCounter = Transaction::select()
        ->when($keyword, function ($query, $keyword) {
            return $query->where('transaction_number', 'like', '%'. $keyword . '%');
        })
        ->count();
        $response = [
            'status'          => true,
            'code'            => '',
            'message'         => '',
            'draw'            => $request['draw'],
            'recordsTotal'    => Transaction::count(),
            'recordsFiltered' => $transactionsCounter,
            'data'            => $transactions,
        ];
        return $response;
    }
    public function create() 
    {
        return view('transaction.create');
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Data gagal ditambahkan'];
            $create = Transaction::create([
                'transaction_number' => Transaction::transaction_number(),
                'date'               => $request['date'],
                'customer_id'        => $request['customer_id'],
                'product_id'         => $request['product_id'],
                'weight'             => $request['weight'],
                'amount'             => str_replace('.', '', $request['amount']),
                'note'               => $request['note'],
            ]);
            if($create){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Data berhasil ditambahkan'];
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem '.$ex];
        }
        return $data;
    }
    public function show($id)
    {
        try {
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Data gagal didapatkan'];
            $user = Transaction::with(['customer', 'product'])->where('id', $id)->first();
            if ($user) {
                $data = ['status'=> true, 'code'=> 'EEC001', 'message'=> 'Data berhasil didapatkan','data'=> $user];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
    public function detail($id) 
    {
        return view('transaction.detail', compact('id'));
    }
    public function edit($id) 
    {
        return view('transaction.edit', compact('id'));
    }
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Data gagal diperbaharui'];
            $update = Transaction::where('id', $request['id'])->update([
                'date'               => $request['date'],
                'customer_id'        => $request['customer_id'],
                'product_id'         => $request['product_id'],
                'weight'             => $request['weight'],
                'amount'             => str_replace('.', '', $request['amount']),
                'note'               => $request['note'],
            ]);
            if($update){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Data berhasil diperbaharui'];
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem '.$ex];
        }
        return $data;
    }
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Data gagal dihapus'];
            
            $delete         = Transaction::where('id', $id)->delete();
            if($delete){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Data berhasil dihapus'];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
    public function finish($id)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Data gagal diselesaikan'];
            
            $finish = Transaction::where('id', $id)->update([
                'status' => true
            ]);
            if($finish){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Data berhasil diselesaikan'];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
}
