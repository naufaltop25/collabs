<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function index() 
    {
        return view('customer.index');
    }
    public function getData(Request $request){
        $keyword = $request['searchkey'];

        $customers = Customer::select()
            ->offset($request['start'])
            ->limit(($request['length'] == -1) ? Customer::count() : $request['length'])
            ->when($keyword, function ($query, $keyword) {
                return $query->where('name', 'like', '%'. $keyword . '%');
            })
            ->latest()
            ->get();

        $customersCounter = Customer::select()
        ->when($keyword, function ($query, $keyword) {
            return $query->where('name', 'like', '%'. $keyword . '%');
        })
        ->count();
        $response = [
            'status'          => true,
            'code'            => '',
            'message'         => '',
            'draw'            => $request['draw'],
            'recordsTotal'    => Customer::count(),
            'recordsFiltered' => $customersCounter,
            'data'            => $customers,
        ];
        return $response;
    }
    public function create() 
    {
        return view('customer.create');
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Data gagal ditambahkan'];
            $create = Customer::create([
                'name'         => $request['name'],
                'email'        => $request['email'],
                'phone_number' => $request['phone_number'],
                'gender'       => $request['gender'],
                'address'      => $request['address'],
            ]);
            if($create){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Data berhasil ditambahkan'];
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem '.$ex];
        }
        return $data;
    }
    public function edit($id) 
    {   
        $customer = Customer::where('id', $id)->first();
        return view('customer.edit', compact('id','customer'));
    }
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Data gagal diperbaharui'];
            $update = Customer::where('id', $request['id'])->update([
                'name'         => $request['name'],
                'email'        => $request['email'],
                'phone_number' => $request['phone_number'],
                'gender'       => $request['gender'],
                'address'      => $request['address'],
            ]);
            if($update){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Data berhasil diperbaharui'];
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem '.$ex];
        }
        return $data;
    }
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Pelanggan gagal dihapus'];
            
            $delete         = Customer::where('id', $id)->delete();
            if($delete){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Pelanggan berhasil dihapus'];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
}
