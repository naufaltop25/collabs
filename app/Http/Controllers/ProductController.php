<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index() {
        return view('product.index');
    }
    public function getData(Request $request){
        $keyword = $request['searchkey'];

        $products = Product::select()
            ->offset($request['start'])
            ->limit(($request['length'] == -1) ? Product::count() : $request['length'])
            ->when($keyword, function ($query, $keyword) {
                return $query->where('name', 'like', '%'. $keyword . '%');
            })
            ->oldest('name')
            ->get();

        $productsCounter = Product::select()
        ->when($keyword, function ($query, $keyword) {
            return $query->where('name', 'like', '%'. $keyword . '%');
        })
        ->count();
        $response = [
            'status'          => true,
            'code'            => '',
            'message'         => '',
            'draw'            => $request['draw'],
            'recordsTotal'    => Product::count(),
            'recordsFiltered' => $productsCounter,
            'data'            => $products,
        ];
        return $response;
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data     = ['status' => false, 'code' => 'EC001', 'message' => 'Produk gagal ditambah'];
            $fileName = Str::random(20);
            $path     = 'images/product/';
            $validator = Validator::make($request->all(), [
                'photo' => 'image|mimes:jpg,jpeg,png|max:2048',
            ], [
                'photo' => 'File tidak boleh lebih dari 2MB, dengan format jpg, jpeg, png',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'code' => 'EC001', 'message' => 'File tidak boleh lebih dari 2MB, dengan format jpg, jpeg, png']);
            }
            if($request->file('photo') != null){
                $extension = $request->file('photo')->extension();
                $photoName = $fileName.'.'.$extension;
                Storage::disk('public')->putFileAs($path,$request->file('photo'), $fileName.".".$extension);
            }else{
                $photoName = null;
            }

            $product              = new Product;
            $product->name        = $request['name'];
            $product->price    = $request['price'];
            $product->note       = $request['note'];
            $product->photo       = $photoName;
            $product->save();

            if($product){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Produk berhasil ditambah'];
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem '.$ex];
        }
        return $data;
    }
    public function show($id)
    {
        try {
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Produk gagal didapatkan'];
            $product = Product::where('id', $id)->first();
            if ($product) {
                $data = ['status'=> true, 'code'=> 'EEC001', 'message'=> 'Produk berhasil didapatkan','data'=> $product];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $data     = ['status' => false, 'code' => 'EC001', 'message' => 'Produk gagal diperbaharui'];
            $fileName = Str::random(20);
            $path     = 'images/product/';

            $validator = Validator::make($request->all(), [
                'photo' => 'image|mimes:jpg,jpeg,png|max:2048',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => 'File tidak boleh lebih dari 2MB, dengan format jpg, jpeg, png']);
            }
            if($request->file('photo') != null){
                $extension = $request->file('photo')->extension();
                $photoName = $fileName.'.'.$extension;
                Storage::disk('public')->putFileAs($path,$request->file('photo'), $fileName.".".$extension);
            }else{
                $photoName = $user->photo;
            }

            $product           = Product::find($request->id);
            $product->name        = $request['name'];
            $product->price    = $request['price'];
            $product->note       = $request['note'];
            $product->photo       = $photoName;
            $product->save();

            if($product){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Produk berhasil diperbaharui'];
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $data = ['status' => false, 'code' => 'EC001', 'message' => 'Produk gagal dihapus'];
            
            $product         = Product::find($id);
            $product->delete();
            if($product){
                DB::commit();
                $data = ['status' => true, 'code' => 'SC001', 'message' => 'Produk berhasil dihapus'];
            }
        } catch (\Exception $ex) {
            $data = ['status' => false, 'code' => 'EEC001', 'message' => 'Ups, Terjadi kesalahan sistem'.$ex];
        }
        return $data;
    }
}
