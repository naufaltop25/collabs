<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
use App\Product;
use App\Transaction;

class DashboardController extends Controller
{
    public function index() {
        $jmlPengguna = User::all()->count();
        $customer = Customer::take(5)->get();
        $product = Product::take(6)->get();
        
        $jmlProduct = Product::all()->count();
        $jmlTransaction = Transaction::where('status', true)->sum('amount');

        return view('dashboard', compact('jmlPengguna', 'customer', 'product', 'jmlProduct', 'jmlTransaction'));
    }
}
