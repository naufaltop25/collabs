<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'transaction_number', 'date', 'weight', 'amount', 'note', 'status', 'customer_id', 'product_id'
    ];

    public static function transaction_number()
    {
        $q = DB::table('transactions')->select(DB::raw('MAX(SUBSTRING(transaction_number,10,4)) as transaction_number_max'));

        if ($q->count() > 0) {
            foreach ($q->get() as $k) {
                $tmp = ((int) $k->transaction_number_max) + 1;
                $transaction_number = "TRX".date('dmy')."".sprintf("%04s", $tmp);
            }
        } else {
            $transaction_number = "TRX".date('dmy')."0001";
        }
        return $transaction_number;
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id', 'id');
    }
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
}
