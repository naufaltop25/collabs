<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'address', 'phone_number', 'email', 'gender',
    ];

    public function transactions()
    {
        return $this->hasMany('App\Transaction', 'customer_id', 'id');
    }
}
