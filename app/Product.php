<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = ['name', 'price', 'note', 'photo'];

    public function transactions()
    {
        return $this->hasMany('App\Transaction', 'product_id', 'id');
    }
}
